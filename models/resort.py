# -*- coding: utf-8 -*-
from odoo import models, fields, api
import datetime



class malfa_resort(models.Model):
    _name = 'malfa.resort'
    
    @api.one
    @api.depends('unit_ids')
    def _get_nbunits(self):
        self.nb_saved_units = len(self.unit_ids)
                 
    name      = fields.Char('Name')
    type      = fields.Selection([
                                 ('istiraha','Istiraha'),
                                 ('chalet','Chalet'),
                                 ('resort','Resort'),
                                 ],default='istiraha',string='Type')
    nb_units  = fields.Integer( string='Number of units',help="Type the number of Units for this resort")
    nb_saved_units  = fields.Integer( compute='_get_nbunits',string='Number of units')
    color     = fields.Integer(string='Color Index')
    unit_ids  = fields.One2many('malfa.unit', 'resort_id','Units')
    web_map  = fields.Char('web map')
    
class resort_unit(models.Model):
    _name = 'malfa.unit'
    
    @api.one
    @api.depends('company_id')
    def _compute_currency(self):
        self.currency_id = self.company_id.currency_id or self.env.user.company_id.currency_id
    
    
    name          = fields.Char('Name')
    image         = fields.Binary("Image", attachment=True)
    company_id    = fields.Many2one('res.company', string='Company', required=True, readonly=True, default=lambda self: self.env.user.company_id)
    currency_id   = fields.Many2one('res.currency', compute='_compute_currency', store=True, string="Currency")
    resort_id     = fields.Many2one('malfa.resort', string="Resort")
    #>>>>>>>>>>>>>>>>>>>>  General details <<<<<<<<<<<<<<<<<<
    unit_number   = fields.Integer('Number')
    entree_date   = fields.Float('Entree date')
    exit_date     = fields.Float('Exit date')
    day_entree_date     = fields.Float('Day entree time')
    day_exit_date = fields.Float('Day Exit time')
    night_entree_date   = fields.Float('Night entree date')
    night_exit_date     = fields.Float('Night Exit date')
    section       = fields.Selection([
                                         ('one','One section'),
                                         ('two','Two sections'),
                                         ],string='Sections ' )
    space                   = fields.Integer(u'Space(m²)',help=u"entre space with m²", default='')
    usage_single            = fields.Boolean(string='Singles')
    usage_families          = fields.Boolean(string='Families')
    usage_parties           = fields.Boolean(string='Parties and socail ocasions')
    usage_sleeping          = fields.Boolean(string='Sleeping')
    #>>>>>>>>>>>>>>>>>>>>  Amenities Info <<<<<<<<<<<<<<<<<<
    common_amenitie_ids     = fields.Many2many('unit.amenities.common','unit_common_amenitie_rel','unit_id','amenitie_id', string="Common Amenities")
    additional_amenitie_ids = fields.Many2many('unit.amenities.additional','unit_additional_amenitie_rel','unit_id','amenitie_id', string="Additional Amenities")
    other_amenitie          = fields.Html('Other Amenitie')
    #>>>>>>>>>>>>>>>>>>>>  Images <<<<<<<<<<<<<<<<<<<<<<<<<<
    image_ids       = fields.One2many('unit.image', 'unit_id','Images')
    #>>>>>>>>>>>>>>>>>>>>  Pricing Details <<<<<<<<<<<<<<<<<
    aid_price       = fields.Monetary(string='Aid price')
    week_day_price  = fields.Monetary(string='Week days price')
    thursday_price  = fields.Monetary(string='Thursday  price')
    friday_price    = fields.Monetary(string='Friday  price')
    saturday_price  = fields.Monetary(string='saturday price')
    rent_half_day   = fields.Boolean(string='Rent for half day')
    day_price       = fields.Monetary(string='Day price')
    night_price     = fields.Monetary(string='Night price')
    pricing_ids     = fields.One2many('unit.price', 'unit_id','Pricing')
    #>>>>>>>>>>>>>>>>>>>>  Humain ressources <<<<<<<<<<<<<<<<<
    agent_id        = fields.Many2one('res.users', string="Agent")
    labor_id        = fields.Many2one('res.users', string="Labor")
    
    @api.onchange('resort_id','unit_number')
    def on_change_unit_number(self):
        if self.resort_id:
            self.name=self.resort_id.name+'_unit_'+str(self.unit_number)
    
    

class unit_image(models.Model):
    _name = 'unit.image'
    
    name      = fields.Char('Name')
    image     = fields.Binary("Image", attachment=True)
    unit_id = fields.Many2one('malfa.unit', string="Unit")

class unit_common_amenities(models.Model):
    _name = 'unit.amenities.common'
    
    name      = fields.Char('Name')
    note     = fields.Html("Description" )

class unit_additional_amenities(models.Model):
    _name = 'unit.amenities.additional'
    
    name      = fields.Char('Name')
    note     = fields.Html("Description" )
    
class unit_price(models.Model):
    _name = 'unit.price'
    
    
    name      = fields.Char('Reason')
    date      = fields.Date("Date")
    price     = fields.Monetary(string='Price')
    unit_id = fields.Many2one('malfa.unit', string="Unit")
    currency_id = fields.Many2one('res.currency', related='unit_id.currency_id', store=True, string="Currency")

