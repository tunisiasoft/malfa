var marker;

function initMapwebsite() {
	 
	 lat=24.915123
	 lng=46.604609
	var map = new google.maps.Map(document.getElementById('map'), {
		  center: new google.maps.LatLng(lat, lng),
		  zoom: 10
	});
    
	
	  google.maps.event.addListener(map, 'click', function(event) {
		  console.log('---------------------------------event',event);
			  if(marker){
				  marker.setMap(null);
				  }
			  marker = new google.maps.Marker({
			   		 position: event.latLng,
			    	 map: map,
		  		});
			  
			  $('input.web_map').val(event.latLng.lat().toString()+","+event.latLng.lng().toString() ); 
		 
	  });
	  
		 
    }
	function getLocationwebsite() {
		    if (navigator.geolocation) {
		        navigator.geolocation.getCurrentPosition(showPositionwebsite);
		    	}
	}
	function getmapwebsite() {  
		if ($("#map").hasClass("hide")){
			$('#map').removeClass( "hide" );
			initMapwebsite();
		}else{
			$('#map').addClass( "hide" );
		}
		
	    
	}
	function showPositionwebsite(position) {
	    $('input.web_map').val(position.coords.latitude.toString()+","+position.coords.longitude.toString() ); 
	}
