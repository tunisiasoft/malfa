var tour = new Tour({
	  debug: true,
	  storage: false,
	  steps: [
			{
				element: ".o-kanban-button-new",
				title: "Create Resorts",
			    content: "Click here to add new resort",
			    placement: "bottom"
			},
			
	]});
if (tour.ended()) {
	  tour.restart();
	} else {
	  tour.init();
	  tour.start();
	}

	
