var marker;

$('.o_form_button_save').click(function() {
	if (!$("#map").hasClass("hide")){
		$('#map').addClass( "hide" );
	}
});
function initMap() {
	 var obj = $('.web_map').val();
	 if(obj.length === 0){
		 obj = $('.web_map').html();
	 }
	 lat=24.915123
	 lng=46.604609
	 if(obj.length != 0){
		 
		 lat=obj.split(',')[0]
		 lng=obj.split(',')[1]
	 }
	var map = new google.maps.Map(document.getElementById('map'), {
		  center: new google.maps.LatLng(lat, lng),
		  zoom: 10
	});
    
	if(obj.length != 0){
      var point = new google.maps.LatLng(
          parseFloat(obj.split(',')[0]),
          parseFloat(obj.split(',')[1]));
	}
      var marker1 = new google.maps.Marker({
          map: map,
          position: point,
        });
	
	  google.maps.event.addListener(map, 'click', function(event) {
		  if($('.name').val()){
			  if(marker1){
			  marker1.setMap(null);
			  }
			  if(marker){
				  marker.setMap(null);
				  }
			  marker = new google.maps.Marker({
			   		 position: event.latLng,
			    	 map: map,
		  		});
			  $('input.web_map').val(event.latLng.lat().toString()+","+event.latLng.lng().toString() ); 
		  }
		 
	  });
	  
		 
    }
function getLocation() {
	if($('.name').val()){
	    if (navigator.geolocation) {
	        navigator.geolocation.getCurrentPosition(showPosition);
	    }
	}
	}
	function getmap() {
		if ($("#map").hasClass("hide")){
			$('#map').removeClass( "hide" );
			initMap();
		}else{
			$('#map').addClass( "hide" );
		}
		
	    
	}
	function showPosition(position) {
	    $('input.web_map').val(position.coords.latitude.toString()+","+position.coords.longitude.toString() ); 
	}
