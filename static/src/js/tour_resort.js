odoo.define('malfa.tour', function (require) {
"use strict";

	var core = require('web.core');
	
	var _t   = core._t;
	var steps=[]
	if($('input.name').length){
	steps=[
			{
				element: "input.name",
				title: _t("Resort name"),
			    content: _t("type the resort name"),
			    placement: "bottom"
			},
			{
				element: "select.type",
				title: _t("Resort type"),
			    content: _t("Choose the resort type"),
			    placement: "bottom"
			},
			{
				element: "input.nb_units",
				title: _t("Number of units"),
			    content: _t("Tape the number of units in this resort"),
			    placement: "bottom"
			},
			{
				element: ".point_map",
				title: _t("Choose from map"),
			    content: _t("Click here to choose your the resort location from map"),
			    placement: "right"
			},
			{
				element: ".share_location",
				title: _t("Share your location"),
			    content: _t("Click here if your current location is the same as the resort location"),
			    placement: "right"
			},
			{
				element: ".generate_unit",
				title: _t("Add resort details"),
			    content: _t("Click here to add resort details"),
			    placement: "right"
			},
		
		
		]
	}
	var tour = new Tour({
	  debug: true,
	  storage: false,
	  template:_t(`<div class="popover tour-tour" >
		  			<div class="arrow" ></div> <h3 class="popover-title" >
			  			</h3> 
			  			<div class="popover-content"></div> 
			  			<div class="popover-navigation"> 
			  			<div class="btn-group"> 
				  			<button class="btn btn-sm btn-default" data-role="prev">« Prev</button> 
				  			<button class="btn btn-sm btn-default" data-role="next">Next »</button>  
			  			</div> 
			  			<button class="btn btn-sm btn-default" data-role="end">End tour</button> 
		  			</div> 
		  		</div>`),
	  steps:steps
	  
	});
	var tour2 = new Tour({
	  debug: true,
	  storage: false,
	  steps: [
	{
		element: ".units_button",
		title: "Title of my step",
	    content: "Click here to change resort details",
	    placement: "right"
	},
	{
	    element: "#my-element",
	    title: "Title of my step",
	    content: "Content of my step",
	    placement: "right"
	  },
	  {
	    element: "#my-other-element",
	    title: "Title of my step",
	    content: "Content of my step",
	    placement: "right"
	  }
	  ]});
	// Initialize the tour
if (tour.ended()) {
	  tour.restart();
	} else {
	  tour.init();
	  tour.start();
	}
});
