{
    "name": "Resort Management",
    "version": "1.0",
    "author" : 'Helmi Dhaoui',
    'website' : 'http://tunisiasoft-tn.com',
    "depends": [
        'auth_signup_verify_email','web_tour','website'
    ],
    "category": "Management",
    "data": [
        "security/resort_security.xml",
        "security/ir.model.access.csv",
        "data/resort_data.xml",
        "wizard/create_units_views.xml",
        "wizard/message_view.xml",
        "views/layout.xml",
        "views/home.xml",
        "views/resort_view.xml",
        "views/assets.xml",
        "views/registre_user.xml",
        "views/add_resort_website_view.xml",
        "views/add_unit_view.xml",
        "views/add_amenitie_view.xml",
        "views/add_amenitie_additional_view.xml",
        "views/add_image_view.xml",
        "views/add_pricing_view.xml",
        "views/add_confirm_view.xml",
        
        "data/malfa_tour.xml",
    ],
    'installable': True,
}
