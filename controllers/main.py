# -*- coding: utf-8 -*-

from odoo.addons.auth_signup.controllers.main import AuthSignupHome
from odoo.http import request
from odoo import http
import base64
import werkzeug.utils
from odoo.addons.website.controllers.main import  Website
from odoo.addons.auth_signup.controllers.main import AuthSignupHome

class AuthSignupHomeAddPhone(AuthSignupHome):
    
    def do_signup(self, qcontext):
        values = { key: qcontext.get(key) for key in ('login', 'name', 'password','phone') }
        assert values.values(), "The form was not properly filled in."
        assert values.get('password'), "You did not specify your phone number."
        assert values.get('password') == qcontext.get('confirm_password'), "Passwords do not match; please retype them."
        supported_langs = [lang['code'] for lang in request.env['res.lang'].sudo().search_read([], ['code'])]
        if request.lang in supported_langs:
            values['lang'] = request.lang
        self._signup_with_values(qcontext.get('token'), values)
        request.env.cr.commit()
        
class AuthSignupMalfa(AuthSignupHome):
    
    @http.route('/web/reset_password', type='http', auth='public', website=True)
    def web_auth_reset_password(self, *args, **kw):
        response = super(AuthSignupMalfa, self).web_auth_reset_password( *args, **kw)
        print
        if request.session.uid:
            if request.env['res.users'].browse(request.uid).has_group('malfa.resort_user'):
                if request.session.get("add"):
                    redirect = '/add_resort'
                else:
                    redirect = '/'
            elif request.env['res.users'].browse(request.uid).has_group('base.group_user'):
                redirect = '/web?' + request.httprequest.query_string
            
            else:
                redirect = '/'
            return http.redirect_with_hash(redirect)
        return response
    
class Website_malfa(Website):
    
    @http.route(website=True, auth="public")
    def web_login(self, redirect=None, *args, **kw):
        response = super(Website, self).web_login(redirect=redirect, *args, **kw)
        a=request.session
        if not redirect and request.params['login_success']:
            if request.env['res.users'].browse(request.uid).has_group('malfa.resort_user'):
                if request.session.get("add"):
                    redirect = '/add_resort'
                else:
                    redirect = '/'
            elif request.env['res.users'].browse(request.uid).has_group('base.group_user'):
                redirect = '/web?' + request.httprequest.query_string
            
            else:
                redirect = '/'
            return http.redirect_with_hash(redirect)
        return response
    
class FormData(http.Controller):
    
     
                                  
    @http.route('/add_resort', auth='public', website=True)
    def addResort(self, **kw):
        if not request.session.uid:
            request.session['add']=True
            return werkzeug.utils.redirect('/web/login', 303)
        return http.request.render('malfa.add_resort', {})



    @http.route('/entreprise_form', auth='public', website=True)
    def form_data(self, **args):
        if not request.session.uid:
            request.session['add']=True
            return werkzeug.utils.redirect('/web/login', 303)
        request.session['name']=args.get("name")
        request.session['type']=args.get("type")
        request.session['nb_units']=int(args.get("nb_units")) if args.get("nb_units") else 0
        request.session['web_map']=args.get("web_map")
        return http.request.render('malfa.entreprise_form_unit', {})
    
    
    @http.route('/entreprise_form_unit', auth='public', website=True)
    def form_data_unit(self, **args):
        if not request.session.uid:
            request.session['add']=True
            return werkzeug.utils.redirect('/web/login', 303)
        request.session['section']=args.get('section')
        request.session['space']=int(args.get('space'))
        request.session['usage_single']="usage_single" in args
        request.session['usage_families']="usage_families" in args 
        request.session['usage_parties']="usage_parties" in args
        request.session['usage_sleeping']="usage_sleeping" in args
        common_amenitie = http.request.env['unit.amenities.common'].search([])
        return http.request.render('malfa.entreprise_form_amenities', {
            'common_amenitie':common_amenitie,
            
            })
    
    @http.route('/entreprise_form_amenities', auth='public', website=True,csrf=False)
    def form_data_amenities(self, **args):
        if not request.session.uid:
            request.session['add']=True
            return werkzeug.utils.redirect('/web/login', 303)
        common_amenitie=[]
        common_amenitie_ids = http.request.env['unit.amenities.common'].search([])
        for common in common_amenitie_ids:
            if 'common'+str(common.id) in args:
                common_amenitie.append(common.id)
        request.session['common_amenitie']=common_amenitie
        additional_amenitie = http.request.env['unit.amenities.additional'].search([])
        return http.request.render('malfa.entreprise_form_amenities_additional', {
            'additional_amenitie':additional_amenitie,
            })
    
    @http.route('/entreprise_form_amenities_additional', auth='public', website=True,csrf=False)
    def form_data_amenities_additional(self, **args):
        if not request.session.uid:
            request.session['add']=True
            return werkzeug.utils.redirect('/web/login', 303)
        additional_amenitie=[]
        additional_amenitie_ids = http.request.env['unit.amenities.additional'].search([])
        for additional in additional_amenitie_ids:
            if 'additional'+str(additional.id) in args:
                additional_amenitie.append(additional.id)
        request.session['additional_amenitie']=additional_amenitie
        return http.request.render('malfa.entreprise_form_images', {})
    
        
    
    @http.route('/entreprise_form_images', auth='public', website=True,csrf=False)
    def form_data_images(self, **args):
        if not request.session.get('image'):
            print '--yes'
            request.session['image']=[]
        if args.get('file_name'):
            for unit_image in request.session.get('image'):
                if  str(args.get('file_name')) in unit_image:
                    
                    request.session.get('image').remove(unit_image)
            return http.request.render('malfa.entreprise_form_images', {})
        if args.get('file'):
            my_file=args.get('file')
            dict_file={
                str(my_file.filename):base64.b64encode(my_file.read())
                }
            
            request.session['image']+=[dict_file]
            return http.request.render('malfa.entreprise_form_images', {})
        else:
            request.session['other_amenitie']=args.get('other_amenitie')
            return http.request.render('malfa.entreprise_form_pricing', {'title_step':u"Step 6 : Define pricing and working time"})
        
    
    def get_date(self,value):
        
        if value:
            float_time_pair = value.split(":")
            if len(float_time_pair) != 2:
                print 'invalid format'
            hours = int(float_time_pair[0])
            minutes = int(float_time_pair[1])
            return hours+float(minutes) /60
        else:
            return False
         
    @http.route('/entreprise_form_pricing', auth='public', website=True)
    def form_data_princing(self, **args):
        if not request.session.uid:
            request.session['add']=True
            return werkzeug.utils.redirect('/web/login', 303)
        request.session['week_day_price']=float(args.get('week_day_price')) if args.get('week_day_price') else 0
        request.session['thursday_price']=float(args.get('thursday_price')) if args.get('thursday_price') else 0
        request.session['friday_price']=float(args.get('friday_price')) if args.get('friday_price') else 0
        request.session['saturday_price']=float(args.get('saturday_price')) if args.get('saturday_price') else 0
        request.session['aid_price']=float(args.get('aid_price')) if args.get('aid_price') else 0
        request.session['day_price']=float(args.get('day_price')) if args.get('day_price') else 0
        request.session['night_price']=float(args.get('night_price')) if args.get('night_price') else 0
        request.session['rent_half_day']="rent_half_day" in args
        request.session['entree_date']=self.get_date(args.get('entree_date'))
        request.session['exit_date']=self.get_date(args.get('exit_date')) 
        request.session['day_entree_date']=self.get_date(args.get('day_entree_date')) 
        request.session['day_exit_date']=self.get_date(args.get('day_exit_date')) 
        request.session['night_entree_date']=self.get_date(args.get('night_entree_date')) 
        request.session['night_exit_date']=self.get_date(args.get('night_exit_date')) 
        name=http.request.env['res.users'].sudo().browse(request.session.uid).partner_id.name
        return http.request.render('malfa.entreprise_form_confirm', {})
    
    @http.route('/entreprise_form_confirm', auth='public', website=True,csrf=False)
    def form_data_confirm(self, **args):
        if not request.session.uid:
            request.session['add']=True
            return werkzeug.utils.redirect('/web/login', 303)
        resort_obj =  http.request.env['malfa.resort']
        unit_obj =  http.request.env['malfa.unit']
        unit_image_obj =  http.request.env['unit.image']
        vals_resort={
            'name'      :request.session.get('name'),
            'type'      :request.session.get('type'),
            'nb_units'  :request.session.get('nb_units'),
            'web_map'   :request.session.get('web_map'),
            }
        resort= resort_obj.create(vals_resort)
        img=''
        if request.session.get('image'):
            key=request.session.get('image')[0].keys()[0]
            img=request.session.get('image')[0][key]
            request.session.get('image').remove(request.session.get('image')[0])
        val={
                'image'                  :img,
                'resort_id'              :resort.id,
                'space'                  :request.session.get('space'),
                'section'                :request.session.get('section'),
                'usage_single'           :request.session.get('usage_single'),
                'usage_families'         :request.session.get('usage_families'),
                'usage_parties'          :request.session.get('usage_parties'),
                'usage_sleeping'         :request.session.get('usage_sleeping'),
                'common_amenitie_ids'    :[(6, 0, request.session.get('common_amenitie'))],
                'additional_amenitie_ids':[(6, 0, request.session.get('additional_amenitie'))],
                'entree_date'            :request.session.get('entree_date'),
                'exit_date'              :request.session.get('exit_date'),
                'day_entree_date'        :request.session.get('day_entree_date'),
                'day_exit_date'          :request.session.get('day_exit_date'),
                'night_entree_date'      :request.session.get('night_entree_date'),
                'night_exit_date'        :request.session.get('night_exit_date'),
                'other_amenitie'         :request.session.get('other_amenitie'),
                'week_day_price'         :request.session.get('week_day_price'),
                'aid_price'              :request.session.get('aid_price'),
                'thursday_price'         :request.session.get('thursday_price'),
                'friday_price'           :request.session.get('friday_price'),
                'saturday_price'         :request.session.get('saturday_price'),
                'rent_half_day'          :request.session.get('rent_half_day'),
                'day_price'              :request.session.get('day_price'),
                'night_price'            :request.session.get('night_price'),
            }
        
        for i in range(request.session.get('nb_units')):
            val['unit_number']=i+1
            val['name']       =resort.name+"_unit_"+str(i+1)
            unit=unit_obj.create(val)
            for unit_image in request.session.get('image'):
                key=unit_image.keys()[0]
                img=unit_image[key]
                image_val={
                        'image':img,
                        'unit_id':unit.id,
                        }
                unit_image_obj.create(image_val)
        request.session.pop('name')
        request.session.pop('type')
        request.session.pop('nb_units')
        request.session.pop('web_map')
        request.session.pop('image')
        request.session.pop('space')
        request.session.pop('section')
        request.session.pop('usage_single')    
        request.session.pop('usage_families')
        request.session.pop('usage_parties')
        request.session.pop('usage_sleeping')
        request.session.pop('common_amenitie')
        request.session.pop('additional_amenitie')
        request.session.pop('entree_date')
        request.session.pop('exit_date')
        request.session.pop('day_entree_date')
        request.session.pop('day_exit_date')
        request.session.pop('night_entree_date')
        request.session.pop('night_exit_date')
        request.session.pop('other_amenitie')
        request.session.pop('week_day_price')
        request.session.pop('aid_price')
        request.session.pop('thursday_price')
        request.session.pop('friday_price')
        request.session.pop('saturday_price')
        request.session.pop('rent_half_day')
        request.session.pop('day_price')
        request.session.pop('night_price')
        return http.request.render('website.homepage', {})