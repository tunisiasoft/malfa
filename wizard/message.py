# -*- coding: utf-8 -*-

from odoo import api, fields, models, _


class malfa_message(models.TransientModel):
    _name = "malfa.message"
    _description = "Pop up for displaying messages"

        
    name          = fields.Char('Name')

