# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

import time

from odoo import api, fields, models, _
from odoo.exceptions import UserError


class create_units(models.TransientModel):
    _name = "create.units"
    _description = "Create and duplicate units"

    @api.one
    @api.depends('company_id')
    def _compute_currency(self):
        self.currency_id = self.company_id.currency_id or self.env.user.company_id.currency_id
    @api.one
    def _get_default(self):
        a=self.env.cotext
        return False
    create_edit   = fields.Selection([
                                         ('create','Create'),
                                         ('edit','Edit'),
                                         ],string='Create or Edit ')
    image         = fields.Binary("Image", attachment=True,default=_get_default)
    company_id    = fields.Many2one('res.company', string='Company', required=True, readonly=True, default=lambda self: self.env.user.company_id)
    currency_id   = fields.Many2one('res.currency', compute='_compute_currency', store=True, string="Currency")
    resort_id     = fields.Many2one('malfa.resort', string="Resort")
    #>>>>>>>>>>>>>>>>>>>>  General details <<<<<<<<<<<<<<<<<<
    unit_number   = fields.Integer('Number')
    entree_date   = fields.Float('Entree date')
    exit_date     = fields.Float('Exit date')
    day_entree_date     = fields.Float('Day entree time')
    day_exit_date = fields.Float('Day Exit time')
    night_entree_date   = fields.Float('Night entree date')
    night_exit_date     = fields.Float('Night Exit date')
    section       = fields.Selection([
                                         ('one','One section'),
                                         ('two','Two sections'),
                                         ],string='Sections ')
    space         = fields.Integer(u'Space(m²)',help=u"entre space with m²", default='')
    usage_single         = fields.Boolean(string='Singles')
    usage_families         = fields.Boolean(string='Families')
    usage_parties         = fields.Boolean(string='Parties and socail ocasions')
    usage_sleeping         = fields.Boolean(string='Sleeping')
    #>>>>>>>>>>>>>>>>>>>>  Amenities Info <<<<<<<<<<<<<<<<<<
    common_amenitie_ids     = fields.Many2many('unit.amenities.common','unit_common_amenitie_rel','unit_id','amenitie_id', string="Common Amenities")
    additional_amenitie_ids = fields.Many2many('unit.amenities.additional','unit_additional_amenitie_rel','unit_id','amenitie_id', string="Additional Amenities")
    other_amenitie          = fields.Html('Other Amenitie')
    #>>>>>>>>>>>>>>>>>>>>  Images <<<<<<<<<<<<<<<<<<<<<<<<<<
    image_ids       = fields.One2many('unit.image', 'unit_id','Images')
    #>>>>>>>>>>>>>>>>>>>>  Pricing Details <<<<<<<<<<<<<<<<<
    aid_price       = fields.Monetary(string='Aid price')
    week_day_price  = fields.Monetary(string='Week days price')
    thursday_price  = fields.Monetary(string='Thursday  price')
    friday_price    = fields.Monetary(string='Friday  price')
    saturday_price  = fields.Monetary(string='saturday price')
    rent_half_day   = fields.Boolean(string='Rent for half day')
    day_price       = fields.Monetary(string='Day price')
    night_price     = fields.Monetary(string='Night price')
    pricing_ids     = fields.One2many('unit.price', 'unit_id','Pricing')
    #>>>>>>>>>>>>>>>>>>>>  Humain ressources <<<<<<<<<<<<<<<<<
    agent_id        = fields.Many2one('res.users', string="Agent")
    labor_id        = fields.Many2one('res.users', string="Labor")
    
    @api.multi
    def create_units(self):
        resort = self.env['malfa.resort'].browse(self._context.get('active_ids', []))
        
        val={
                'image'                  :self.image,
                'resort_id'              :resort.id,
                'section'                :self.section,
                'usage_single'           :self.usage_single,
                'usage_families'         :self.usage_families,
                'usage_parties'          :self.usage_parties,
                'usage_sleeping'         :self.usage_sleeping,
                'entree_date'            :self.entree_date,
                'exit_date'              :self.exit_date,
                'day_entree_date'        :self.day_entree_date,
                'day_exit_date'          :self.day_exit_date,
                'night_entree_date'      :self.night_entree_date,
                'night_exit_date'        :self.night_exit_date,
                'other_amenitie'         :self.other_amenitie,
                'week_day_price'         :self.week_day_price,
                'aid_price'              :self.aid_price,
                'thursday_price'         :self.thursday_price,
                'friday_price'           :self.friday_price,
                'saturday_price'         :self.saturday_price,
                'rent_half_day'          :self.rent_half_day,
                'day_price'              :self.day_price,
                'night_price'            :self.night_price,
                'agent_id'               :self.agent_id.id,
                'labor_id'               :self.labor_id.id,
            }
        if self.create_edit=='create':
            
            for i in range(resort.nb_units):
                val['unit_number']=i+1
                val['name']       =resort.name+"_unit_"+str(i+1)
                unit=self.env['malfa.unit'].create(val)
                for image in self.image_ids:
                    image_val={
                        'image':image.image,
                        'name':image.name,
                        'unit_id':unit.id
                        }
                    unit=self.env['unit.image'].create(image_val)
        else:
            for unit in resort.unit_ids:
                unit.write(val)
                unit.image_ids=[]
                for image in self.image_ids:
                    image_val={
                        'image':image.image,
                        'name':image.name,
                        'unit_id':unit.id
                        }
                    unit=self.env['unit.image'].create(image_val)
        
        imd = self.env['ir.model.data']
        action = imd.xmlid_to_object('malfa.action_view_malfa_message')
        result = {
            'name': action.name,
            'help': action.help,
            'type': action.type,
            'view_mode': 'form',
            'target': action.target,
            'res_model': action.res_model,
        }
        return result
